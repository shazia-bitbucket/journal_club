import numpy as np
import glob
import cPickle
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = (7,7) # Make the figures a bit bigger

from keras import backend as K
K.set_image_dim_ordering('th')

from keras.datasets import mnist
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense

from keras.layers import Input
from keras import applications
from keras.models import Model
from keras.utils import np_utils
from sklearn.utils import shuffle
from sklearn.metrics import confusion_matrix
from scipy.ndimage.interpolation import rotate

def unpickle(file):
    fo = open(file, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict

nb_classes = 10

# the data, shuffled and split between tran and test sets
train_files = glob.glob('./cifar-10-batches/train/*.pkl')
test_files = glob.glob('./cifar-10-batches/test/*.pkl')

Xtrain = np.array([]).astype('float32')
Ytrain = Xtest = Ytest = Xtrain
for tf in train_files:
    a = unpickle(tf)
    x = a['data']
    x = x.reshape(x.shape[0], 3, 32, 32)
    if(len(Xtrain) == 0):
    	Xtrain = x
    else:
    	Xtrain = np.concatenate((Xtrain, x))
    Ytrain = np.concatenate((Ytrain, a['labels']))
for tf in test_files:
    a = unpickle(tf)
    x = a['data']
    x = x.reshape(x.shape[0], 3, 32, 32)
    if(len(Xtest) == 0):
    	Xtest = x
    else:
    	Xtest = np.concatenate((Xtest, x))
    Ytest = np.concatenate((Ytest, a['labels']))

Ytrain = np_utils.to_categorical(Ytrain, nb_classes)
Ytest = np_utils.to_categorical(Ytest, nb_classes)


Xtrain, Ytrain = shuffle(Xtrain, Ytrain, random_state=0)

print(Xtrain.shape, Xtest.shape, Ytrain.shape, Ytest.shape)

for i in range(9):
    plt.subplot(3,3,i+1)
    plt.axis('off')
    plt.imshow(Xtrain[i].transpose(1,2,0), interpolation='none')
    plt.title("Class {}".format(np.argmax(Ytrain[i])))
plt.show()

# Normalize data instances
Xtrain = Xtrain / 255.0
Xtest = Xtest / 255.0

#add the last few layers which we will fine tune
input_ = Input((3,32,32))
vgg16 = applications.VGG16(weights=None, include_top=False)
x = vgg16(input_)
x = Flatten()(x)
x = Dense(1024, activation='relu')(x)
predictions = Dense(nb_classes, activation='softmax')(x)
model = Model(input=input_, output=predictions)

for layer in vgg16.layers:
    layer.trainable = False

# Compile the model
model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.0001), metrics=['accuracy'])

# Train model
history = model.fit(Xtrain, Ytrain,
              batch_size=128, nb_epoch=50,
              validation_data=(Xtest, Ytest))
              
                 
# Summarize history for validation accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

score = model.evaluate(Xtest, Ytest)
print("loss on test set: %.3f, accuracy: %.3f" % (score[0], score[1]))
