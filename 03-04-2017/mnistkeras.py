import numpy as np
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = (7,7) # Make the figures a bit bigger

from keras.datasets import mnist
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.layers.core import Dense, Flatten, Activation, Dropout
from keras.utils import np_utils
from sklearn import datasets
from sklearn.cross_validation import train_test_split
from sklearn.utils import shuffle
from scipy.ndimage.interpolation import zoom

# For a commented version, please refer to the ipython notebook: MNISTKeras.ipynb

nb_classes = 10

# load dataset from sklearn
dataset = datasets.fetch_mldata("MNIST Original")
X = dataset.data
X = X.reshape((X.shape[0], 28, 28))
X = X[:, np.newaxis, :, :]    # add channel
X = X / 255.0
Y = np_utils.to_categorical(dataset.target.astype("int"), nb_classes)

#split into training and test set
Xtrain, Xtest, Ytrain, Ytest = train_test_split(X, Y, test_size=0.3, random_state=1)

# Augment the dataset by scaling each image an additional 4x
augmented_data = np.array([])
zoom_factors = [0.5, 0.75, 1.25, 1.5]
for i in range(len(zoom_factors)):
	zoomed_image = zoom(Xtrain, [1, 1, zoom_factors[i], zoom_factors[i]], mode='constant')
	
	fsh = Xtrain.shape
	zsh = zoomed_image.shape
	if(zsh[-1] > Xtrain.shape[-1]):
		filled_image = zoomed_image[:, :, zsh[-2]/2 - fsh[-2]/2: zsh[-2]/2 + fsh[-2]/2, zsh[-1]/2 - fsh[-1]/2: zsh[-1]/2 + fsh[-1]/2]
	else:
		filled_image = np.zeros(Xtrain.shape)
		filled_image[:,:,fsh[-2]/2 - zsh[-2]/2: fsh[-2]/2 + zsh[-2]/2 + zsh[-2]%2,fsh[-1]/2 - zsh[-1]/2: fsh[-1]/2 + zsh[-1]/2 + zsh[-1]%2] = zoomed_image
		
	if(len(augmented_data) == 0):
		augmented_data = filled_image
	else:
		augmented_data = np.concatenate((augmented_data, filled_image), axis=0)
augmented_data = np.concatenate((augmented_data, Xtrain), axis=0)
Xtrain = augmented_data
Ytrain = np.tile(Ytrain, (len(Xtrain)/len(Ytrain), 1))

Xtrain, Ytrain = shuffle(Xtrain, Ytrain, random_state=0)

print(Xtrain.shape, Xtest.shape, Ytrain.shape, Ytest.shape)

for i in range(9):
    plt.subplot(3,3,i+1)
    plt.axis('off')
    plt.imshow(Xtrain[i,0], cmap='gray', interpolation='none')
    plt.title("Class {}".format(np.argmax(Ytrain[i])))
plt.show()
    
model = Sequential()
# first convolutional layer
model.add(Convolution2D(20, 5, 5, border_mode="same", input_shape=(1, 28, 28)))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2), dim_ordering="th"))
# second convolution layer
model.add(Convolution2D(50, 5, 5, border_mode="same"))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2), dim_ordering="th"))
# first fully connected layer
model.add(Flatten())
model.add(Dense(512))
model.add(Activation("relu"))
model.add(Dropout(0.5))
# second fully connected layer + softmax
model.add(Dense(nb_classes))
model.add(Activation("softmax"))

# Compile the model
model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.0001), metrics=['accuracy'])

# Train model
history = model.fit(Xtrain, Ytrain,
              batch_size=128, nb_epoch=100,
              show_accuracy=True, verbose=1,
              validation_data=(Xtest, Ytest))
              
# Summarize history for validation accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

score = model.evaluate(Xtest, Ytest, show_accuracy=True, verbose=0)
print("loss on test set: %.3f, accuracy: %.3f" % (score[0], score[1]))

# The predict_classes function outputs the highest probability class
# according to the trained classifier for each input example.
predicted_classes = model.predict_classes(Xtest)

# Check which items we got right / wrong
target = np.argmax(Ytest, axis=1)
correct_indices = np.nonzero(predicted_classes == target)[0]
incorrect_indices = np.nonzero(predicted_classes != target)[0]

plt.figure()
for i, correct in enumerate(correct_indices[:9]):
    plt.subplot(3,3,i+1)
    plt.imshow(Xtest[correct].reshape(28,28), cmap='gray', interpolation='none')
    plt.title("Predicted {}, Class {}".format(predicted_classes[correct], np.argmax(Ytest[correct])))
plt.show()
    
plt.figure()
for i, incorrect in enumerate(incorrect_indices[:9]):
    plt.subplot(3,3,i+1)
    plt.imshow(Xtest[incorrect].reshape(28,28), cmap='gray', interpolation='none')
    plt.title("Predicted {}, Class {}".format(predicted_classes[incorrect], np.argmax(Ytest[incorrect])))
plt.show()