###################################################################
# A simple keras implementation of CNN trained on CIFAR-10
#
# Used for journal club session on 3rd April 2017
# Feel free to use this as a baseline for your own experiments
###################################################################

import numpy as np
import glob
import cPickle
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = (7,7) # Make the figures a bit bigger

from keras import backend as K
K.set_image_dim_ordering('th')

from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.layers.core import Dense, Flatten, Activation, Dropout
from keras.utils import np_utils
from sklearn.utils import shuffle
from sklearn.metrics import confusion_matrix
from scipy.ndimage.interpolation import zoom

def unpickle(file):
    fo = open(file, 'rb')
    dict = cPickle.load(fo)
    fo.close()
    return dict

nb_classes = 10

# We need to first load in the data and format it to feed into the keras implementation
# cifar-10 is downloadable here: https://www.cs.toronto.edu/~kriz/cifar.html

# Get the name of each batch into a list 
train_files = glob.glob('./cifar-10-batches/train/*.pkl')
test_files = glob.glob('./cifar-10-batches/test/*.pkl')

# For each file in training and test set, load them in
# Usage is explained in the URL above

Xtrain = np.array([]).astype('float32')
Ytrain = Xtest = Ytest = Xtrain
for tf in train_files:
    a = unpickle(tf)
    x = a['data']
    x = x.reshape(x.shape[0], 3, 32, 32)
    if(len(Xtrain) == 0):
    	Xtrain = x
    else:
    	Xtrain = np.concatenate((Xtrain, x))
    	
    # We also load the labels in here as well
    Ytrain = np.concatenate((Ytrain, a['labels']))
for tf in test_files:
    a = unpickle(tf)
    x = a['data']
    x = x.reshape(x.shape[0], 3, 32, 32)
    if(len(Xtest) == 0):
    	Xtest = x
    else:
    	Xtest = np.concatenate((Xtest, x))
    Ytest = np.concatenate((Ytest, a['labels']))

Ytrain = np_utils.to_categorical(Ytrain, nb_classes)
Ytest = np_utils.to_categorical(Ytest, nb_classes)

# Augment the dataset by scaling each image an additional 4x
# Uncomment if you would like to use this option but it will use up memory so be wary
'''
augmented_data = np.array([])
zoom_factors = [0.5, 0.75, 1.25, 1.5]
for i in range(len(zoom_factors)):
	zoomed_image = zoom(Xtrain, [1, 1, zoom_factors[i], zoom_factors[i]], mode='constant')
	
	fsh = Xtrain.shape
	zsh = zoomed_image.shape
	if(zsh[-1] > Xtrain.shape[-1]):
		filled_image = zoomed_image[:, :, zsh[-2]/2 - fsh[-2]/2: zsh[-2]/2 + fsh[-2]/2, zsh[-1]/2 - fsh[-1]/2: zsh[-1]/2 + fsh[-1]/2]
	else:
		filled_image = np.zeros(Xtrain.shape)
		filled_image[:,:,fsh[-2]/2 - zsh[-2]/2: fsh[-2]/2 + zsh[-2]/2 + zsh[-2]%2,fsh[-1]/2 - zsh[-1]/2: fsh[-1]/2 + zsh[-1]/2 + zsh[-1]%2] = zoomed_image
		
	if(len(augmented_data) == 0):
		augmented_data = filled_image
	else:
		augmented_data = np.concatenate((augmented_data, filled_image), axis=0)
augmented_data = np.concatenate((augmented_data, Xtrain), axis=0)
Xtrain = augmented_data
Ytrain = np.tile(Ytrain, (len(Xtrain)/len(Ytrain), 1))
'''

# Randomly shuffle the dataset
Xtrain, Ytrain = shuffle(Xtrain, Ytrain, random_state=0)

print(Xtrain.shape, Xtest.shape, Ytrain.shape, Ytest.shape)

# Display some of the images
for i in range(9):
    plt.subplot(3,3,i+1)
    plt.axis('off')
    plt.imshow(Xtrain[i].transpose(1,2,0), interpolation='none')
    plt.title("Class {}".format(np.argmax(Ytrain[i])))
plt.show()

# Normalize data
Xtrain = Xtrain / 255.0
Xtest = Xtest / 255.0


# This is the architecture we used when implementating the approach defined by Xudong Cao
# The number of filter units were also increased as the depth was increased.

model = Sequential()

# Stage 1
model.add(Convolution2D(64, 5, 5, border_mode="same", input_shape=(3, 32, 32)))
model.add(Activation("relu"))
model.add(Convolution2D(64, 5, 5, border_mode="same"))
model.add(Activation("relu"))
model.add(Convolution2D(64, 5, 5, border_mode="same"))
model.add(Activation("relu"))
model.add(Convolution2D(64, 5, 5, border_mode="same"))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

# Stage 2 (here the size of the image was 2x2 after pooling so we stop)
model.add(Convolution2D(64, 5, 5, border_mode="same"))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))

# Classification stage:
model.add(Convolution2D(64, 5, 5, border_mode="same"))
model.add(Activation("relu"))
model.add(Convolution2D(32, 5, 5, border_mode="same"))
model.add(Activation("relu"))
model.add(MaxPooling2D(pool_size=(6, 6)))
model.add(Flatten())
model.add(Dropout(p=0.5))
model.add(Dense(nb_classes))
model.add(Activation("softmax"))


# Compile the model using Adam to optimize our model
# lr refers to the learning rate which we discussed at the start of this journal club session
model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=0.001), metrics=['accuracy'])

# Train model
history = model.fit(Xtrain, Ytrain,
              batch_size=128, nb_epoch=50, verbose=1,
              validation_data=(Xtest, Ytest))
              
# Summarize history for validation accuracy and display in a plot graph
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

# Get the accuracy achieved on the test set
score = model.evaluate(Xtest, Ytest)
print("loss on test set: %.3f, accuracy: %.3f" % (score[0], score[1]))

# The predict_classes function outputs the highest probability class
# according to the trained classifier for each input example.
predicted_classes = model.predict_classes(Xtest)

# Check which items we got right / wrong
target = np.argmax(Ytest, axis=1)
correct_indices = np.nonzero(predicted_classes == target)[0]
incorrect_indices = np.nonzero(predicted_classes != target)[0]
print(confusion_matrix(target, predicted_classes))

# Display correct 
plt.figure()
for i, correct in enumerate(correct_indices[:9]):
    plt.subplot(3,3,i+1)
    plt.imshow(Xtest[correct].reshape(3,32,32).transpose(1,2,0)*255.0)
    plt.title("Predicted {}, Class {}".format(predicted_classes[correct], np.argmax(Ytest[correct])))
plt.show()
    
# Display incorrect
plt.figure()
for i, incorrect in enumerate(incorrect_indices[:9]):
    plt.subplot(3,3,i+1)
    plt.imshow(Xtest[incorrect].reshape(3,32,32).transpose(1,2,0)*255.0)
    plt.title("Predicted {}, Class {}".format(predicted_classes[incorrect], np.argmax(Ytest[incorrect])))
plt.show()
